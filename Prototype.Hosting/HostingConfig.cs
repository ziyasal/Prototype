﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HostingConfig.cs" company="Hypless">
//   Hypless
// </copyright>
// <summary>
//   The web api config.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Hosting
{
    using System;
    using System.Linq;
    using System.Web.Http;

    using Prototype.Core;
    using Prototype.Core.Extensions;

    /// <summary>The web API config.</summary>
    public static class HostingConfig
    {
        /// <summary>The register.</summary>
        /// <param name="config">The config.</param>
        public static void Register(HttpConfiguration config)
        {
            // Call all API configurators to populate hosting configuration
            AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(
                    a => a.GetTypes().Where(
                        p => typeof(IApiConfigurator).IsAssignableFrom(p) && p.IsPublic && !p.IsAbstract))
                .Select(p => (IApiConfigurator)Activator.CreateInstance(p)).ForEach(c => { c.Configure(config); });

            config.Routes.MapHttpRoute("DefaultApi", "{controller}/{id}", new { id = RouteParameter.Optional });
        }
    }
}