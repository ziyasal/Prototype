﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IocConfig.cs" company="Hypless">
//   Hypless
// </copyright>
// <summary>
//   Defines the IocConfig type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Hosting
{
    using System.Reflection;
    using System.Web.Http;

    using Autofac;
    using Autofac.Integration.WebApi;

    using Microsoft.Extensions.Logging;

    using Prototype.Api;
    using Prototype.Api.OData;

    /// <summary>The IoC config.</summary>
    public class IocConfig
    {
        /// <summary>The configure.</summary>
        /// <param name="config">The config.</param>
        public static void Configure(HttpConfiguration config)
        {
            var builder = new ContainerBuilder();

            // Api v1.
            // TODO: read env from environment variable
            builder.RegisterModule(new ApiIocModule
            {
                Env = "TEST"
            });

            // OData
            builder.RegisterModule<ODataApiIocModule>();

            // MS Logging
            builder.RegisterType<LoggerFactory>()
                .As<ILoggerFactory>()
                .SingleInstance();

            builder.RegisterGeneric(typeof(Logger<>)).As(typeof(ILogger<>)).SingleInstance();

            var container = builder.Build();

            var loggerFactory = container.Resolve<ILoggerFactory>();

            // TODO: AddNlog() ??
            loggerFactory.AddConsole();

            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}