﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Global.asax.cs" company="Hypless">
//   Hypless
// </copyright>
// <summary>
//   The web api application.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Hosting
{
    using System.Web;
    using System.Web.Http;

    /// <summary>The web API application.</summary>
    public class WebApiApplication : HttpApplication
    {
        /// <summary>The application start.</summary>
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(HostingConfig.Register);

            IocConfig.Configure(GlobalConfiguration.Configuration);
        }
    }
}