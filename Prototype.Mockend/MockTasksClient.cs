﻿namespace Prototype.Mockend
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using AutoFixture;

    using Prototype.Core.Model;
    using Prototype.Core.RestClients;

    public class MockTasksClient : ITasksClient
    {
        public Task<ResponseModel<TaskModel>> GetTasks()
        {
            return Task.FromResult(new Fixture().Create<ResponseModel<TaskModel>>());
        }
    }
}
