﻿namespace Prototype.Mockend
{
    using System;
    using System.Threading.Tasks;

    using AutoFixture;

    using Prototype.Core.Model;
    using Prototype.Core.RestClients;

    public class MockEventsClient : IEventsClient
    {
        public Task<ResponseModel<EventModel>> GetEvents()
        {
            return Task.FromResult(new Fixture().Create<ResponseModel<EventModel>>());
        }
    }
}