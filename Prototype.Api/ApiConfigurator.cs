﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ApiConfigurator.cs" company="Hypless">
//   Hypless
// </copyright>
// <summary>
//   The bus api config.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Api
{
    using System.Net.Http.Formatting;
    using System.Web.Http;
    using System.Web.Http.Routing;

    using ExpressMapper;

    using Microsoft.Web.Http.Routing;

    using Prototype.Core;

    /// <summary>The bus API config.</summary>
    public class ApiConfigurator : IApiConfigurator
    {
        /// <summary>The configure.</summary>
        /// <param name="config">The config.</param>
        public void Configure(HttpConfiguration config)
        {
            var constraintResolver =
                new DefaultInlineConstraintResolver()
                {
                    ConstraintMap =
                            {
                                ["apiVersion"] = typeof(ApiVersionRouteConstraint)
                            }
                };

            config.MapHttpAttributeRoutes(constraintResolver);

            config.AddApiVersioning(
                o =>
                    {
                        o.AssumeDefaultVersionWhenUnspecified = true;
                        o.ReportApiVersions = true;
                    });


            config.Formatters.Clear();
            config.Formatters.Add(new JsonMediaTypeFormatter());
        }
    }
}