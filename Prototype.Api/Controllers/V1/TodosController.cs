﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TodosController.cs" company="Hypless">
//   Hypless
// </copyright>
// <summary>
//   The to-do's controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Api.Controllers.V1
{
    using System;
    using System.Threading.Tasks;
    using System.Web.Http;

    using Autofac.Features.AttributeFilters;

    using Microsoft.Web.Http;

    using Prototype.Core;
    using Prototype.Core.Services;

    /// <summary>The to-do's controller.</summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/todos")]
    public class TodosController : ApiController
    {
        /// <summary>The to-do service.</summary>
        private readonly ITodosService todosService;

        /// <summary>Initializes a new instance of the <see cref="TodosController" /> class.</summary>
        /// <param name="todosService">The to do service.</param>
        public TodosController(ITodosService todosService)
        {
            this.todosService = todosService;
        }

        /// <summary>The get.</summary>
        /// <returns>The <see cref="Task" />.</returns>
        [HttpGet]
        public async Task<IHttpActionResult> Get()
        {
            // omitted for brevity

            var responseOption = await this.todosService.GetTodos();

            return responseOption.Match<IHttpActionResult>(some: this.Ok, none: this.BadRequest);
        }
    }
}