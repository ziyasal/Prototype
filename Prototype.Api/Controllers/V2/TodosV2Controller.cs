﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TodosV2Controller.cs" company="">
//   
// </copyright>
// <summary>
//   The todos v2 controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Api.Controllers.V2
{
    using System;
    using System.Threading.Tasks;
    using System.Web.Http;

    using Autofac.Features.AttributeFilters;

    using Microsoft.Web.Http;

    using Prototype.Core;
    using Prototype.Core.Services;

    /// <summary>The to-do's v2 controller.</summary>
    [ApiVersion("2.0")]
    [Route("api/v{version:apiVersion}/todos")]
    public class TodosV2Controller : ApiController
    {
        /// <summary>The to-do service.</summary>
        private readonly ITodosService todosService;

        /// <summary>Initializes a new instance of the <see cref="TodosV2Controller" /> class.</summary>
        /// <param name="todosService">The to do service.</param>
        public TodosV2Controller(ITodosService todosService)
        {
            this.todosService = todosService;
        }

        /// <summary>The get.</summary>
        /// <returns>The <see cref="Task" />.</returns>
        [HttpGet]
        public async Task<IHttpActionResult> Get()
        {
            // omitted for brevity
            var responseOption = await this.todosService.GetTodos();

            return responseOption.Match<IHttpActionResult>(some: this.Ok, none: this.BadRequest);
        }
    }
}