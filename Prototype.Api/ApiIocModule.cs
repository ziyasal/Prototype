﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ApiIocModule.cs" company="Hypless">
//   Hypless
// </copyright>
// <summary>
//   The bus module.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Api
{
    using System.Web.Http;

    using Autofac;
    using Autofac.Extras.AttributeMetadata;
    using Autofac.Integration.WebApi;

    using ExpressMapper;

    using Microsoft.Extensions.Options;

    using Prototype.Api.Controllers.V1;
    using Prototype.Api.Controllers.V2;
    using Prototype.Core.Auth;
    using Prototype.Core.Contracts.V1;
    using Prototype.Core.Contracts.V1.Mapping;
    using Prototype.Core.Contracts.V2;
    using Prototype.Core.Contracts.V2.Mapping;
    using Prototype.Core.Http;
    using Prototype.Core.Mapping;
    using Prototype.Core.Model;
    using Prototype.Core.RestClients;
    using Prototype.Core.Serialization;
    using Prototype.Core.Services;
    using Prototype.Core.Services.V1;
    using Prototype.Core.Services.V2;
    using Prototype.Core.Versioning;
    using Prototype.Mockend;

    /// <summary>The bus module.</summary>
    public class ApiIocModule : Module
    {
        /// <summary>Gets or sets the env.</summary>
        public string Env { get; set; }

        /// <summary>The load.</summary>
        /// <param name="builder">The builder.</param>
        protected override void Load(ContainerBuilder builder)
        {
            if (this.Env == "TEST")
            {
                builder.RegisterType<MockEventsClient>().As<IEventsClient>().InstancePerDependency();
                builder.RegisterType<MockTasksClient>().As<ITasksClient>().InstancePerDependency();
            }
            else
            {
                builder.RegisterType<EventsClient>().As<IEventsClient>().InstancePerDependency();
                builder.RegisterType<TasksClient>().As<ITasksClient>().InstancePerDependency();
                builder.RegisterType<HttpClientFactory>().As<IHttpClientFactory>().SingleInstance();
            }

            // TODO: read from config/settings.
            builder.RegisterInstance(
                Options.Create(new AppRetryOptions { HandlerEnabled = true, HttpReqRetryCount = 3 }));

            builder.RegisterType<NewtonsoftJsonSerializer>().As<IJsonSerializer>().SingleInstance();
            builder.RegisterType<BearerTokenProvider>().As<ITokenProvider>().SingleInstance();
            builder.RegisterType<HttpContextAccessor>().As<IHttpContextAccessor>().SingleInstance();

            builder.RegisterType<RequestedApiVersionService>().As<IVersionService>().SingleInstance();

            builder.RegisterVersioned<TodosService, ITodosService>();
            builder.RegisterVersioned<TodosV2Service, ITodosService>();

            this.RegisterMappings(builder);

            builder.RegisterVersioned<TodosController>();
            builder.RegisterVersioned<TodosV2Controller>();
        }

        /// <summary>The register mappings.</summary>
        /// <param name="builder">The builder.</param>
        private void RegisterMappings(ContainerBuilder builder)
        {
            builder.RegisterVersionedMapper<MappingServiceProvider, MappingV1Profile>();
            builder.RegisterVersionedMapper<MappingServiceProvider, MappingV2Profile>();
        }
    }
}