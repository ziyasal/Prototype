﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IMappingProfile.cs" company="">
//   
// </copyright>
// <summary>
//   The MappingProfile interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Core.Mapping
{
    using ExpressMapper;

    /// <summary>The MappingProfile interface.</summary>
    public interface IMappingProfile
    {
        /// <summary>The build mapping service provider.</summary>
        /// <returns>The <see cref="IMappingServiceProvider"/>.</returns>
        IMappingServiceProvider GetMappingProvider();
    }
}