﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HttpContextExtensions.cs" company="Hypless">
//   Hypless
// </copyright>
// <summary>
//   The http extensions.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Core.Http
{
    using System.Net.Http;
    using System.Threading;
    using System.Web;

    /// <summary>The http extensions.</summary>
    public static class HttpContextExtensions
    {
        /// <summary>The get cancellation token.</summary>
        /// <param name="request">The request.</param>
        /// <returns>The <see cref="CancellationToken" />.</returns>
        public static CancellationToken GetCancellationToken(this HttpRequestMessage request)
        {
            CancellationToken cancellationToken;
            var key = typeof(HttpContextExtensions).Namespace + "::CancellationToken";

            if (request.Properties.TryGetValue(key, out var value))
            {
                return (CancellationToken)value;
            }

            var httpContext = HttpContext.Current;

            var httpResponse = httpContext?.Response;

            if (httpResponse != null)
            {
                try
                {
                    cancellationToken = httpResponse.ClientDisconnectedToken;
                }
                catch
                {
                    // Do not support cancellation.
                }
            }

            request.Properties[key] = cancellationToken;

            return cancellationToken;
        }
    }
}