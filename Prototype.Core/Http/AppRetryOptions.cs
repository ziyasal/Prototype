﻿namespace Prototype.Core.Http
{
    /// <summary>The app retry options.</summary>
    public class AppRetryOptions
    {
        /// <summary>Gets or sets a value indicating whether handler enabled.</summary>
        public bool HandlerEnabled { get; set; }

        /// <summary>Gets or sets the http req retry count.</summary>
        public int HttpReqRetryCount { get; set; }
    }
}