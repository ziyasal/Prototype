﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IHttpContextAccessor.cs" company="">
//   
// </copyright>
// <summary>
//   The HttpContextAccessor interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Core.Http
{
    using System.Web;

    /// <summary>The HttpContextAccessor interface.</summary>
    public interface IHttpContextAccessor
    {
        /// <summary>Gets the http context.</summary>
        HttpContextWrapper HttpContext { get; }
    }
}