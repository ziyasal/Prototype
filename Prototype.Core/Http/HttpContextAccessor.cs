﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HttpContextAccessor.cs" company="">
//   
// </copyright>
// <summary>
//   The http context accessor.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Core.Http
{
    using System.Web;

    /// <summary>The http context accessor.</summary>
    public class HttpContextAccessor : IHttpContextAccessor
    {
        /// <summary>The http context.</summary>
        public HttpContextWrapper HttpContext => new HttpContextWrapper(System.Web.HttpContext.Current);
    }
}