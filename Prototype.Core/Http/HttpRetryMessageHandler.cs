﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HttpRetryMessageHandler.cs" company="">
//   
// </copyright>
// <summary>
//   The http retry message handler.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Core.Http
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;

    using Microsoft.Extensions.Options;

    using Polly;

    /// <summary>The http retry message handler.</summary>
    public class HttpRetryMessageHandler : DelegatingHandler
    {
        /// <summary>Gets or sets the options.</summary>
        public IOptions<AppRetryOptions> Options { get; set; }

        /// <summary>The send async.</summary>
        /// <param name="request">The request.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>The <see cref="Task" />.</returns>
        [SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1126:PrefixCallsCorrectly", Justification = "Reviewed. Suppression is OK here.")]
        protected override async Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request,
            CancellationToken cancellationToken)
        {
            HttpStatusCode[] httpStatusCodesWorthRetrying =
                {
                    HttpStatusCode.RequestTimeout, // 408
                    HttpStatusCode.InternalServerError, // 500
                    HttpStatusCode.BadGateway, // 502
                    HttpStatusCode.ServiceUnavailable, // 503
                    HttpStatusCode.GatewayTimeout // 504
                };

            TimeSpan SleepDurationProvider(int retryCount) => TimeSpan.FromSeconds(Math.Pow(3, retryCount));

            var httpResponseMessage = await Policy.Handle<HttpRequestException>().Or<TaskCanceledException>()
                                          .OrResult<HttpResponseMessage>(
                                              r => httpStatusCodesWorthRetrying.Contains(r.StatusCode))
                                          .WaitAndRetryAsync(
                                              this.Options.Value.HttpReqRetryCount,
                                              SleepDurationProvider).ExecuteAsync(
                                              async () => await base.SendAsync(request, cancellationToken));

            return httpResponseMessage;
        }
    }
}