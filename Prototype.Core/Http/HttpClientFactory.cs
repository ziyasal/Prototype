﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HttpClientFactory.cs" company="">
//   
// </copyright>
// <summary>
//   The http client factory.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Core.Http
{
    using System.Diagnostics.Tracing;
    using System.Net.Http;
    using System.Net.Http.Headers;

    using Microsoft.Extensions.Options;
    using Microsoft.Web.Http;

    using Optional;

    using Prototype.Core.Versioning;
    using static Optional.Option;

    /// <summary>The http client factory.</summary>
    public class HttpClientFactory : IHttpClientFactory
    {
        /// <summary>The application retry options.</summary>
        private readonly IOptions<AppRetryOptions> appRetryOptions;

        /// <summary>The http context accessor.</summary>
        private readonly IHttpContextAccessor httpContextAccessor;

        /// <summary>The version service.</summary>
        private readonly IVersionService versionService;

        /// <summary>Initializes a new instance of the <see cref="HttpClientFactory" /> class.</summary>
        /// <param name="appRetryOptions">The app retry options.</param>
        /// <param name="httpContextAccessor">The http context accessor.</param>
        /// <param name="versionService">The version service.</param>
        public HttpClientFactory(
            IOptions<AppRetryOptions> appRetryOptions,
            IHttpContextAccessor httpContextAccessor,
            IVersionService versionService)
        {
            this.appRetryOptions = appRetryOptions;
            this.httpContextAccessor = httpContextAccessor;
            this.versionService = versionService;
        }

        /// <summary>The get client.</summary>
        /// <param name="preferredVersion">The preferred Verrsion.</param>
        /// <returns>The <see cref="HttpClient"/>.</returns>
        public HttpClient GetClient(ApiVersion preferredVersion = null)
        {
            // chain: httpclient -> (conditional)[retry_handler] -> httpclient_handler
            var httpMessageHandler = new HttpClientHandler();

            var retryMessageHandler =
                new HttpRetryMessageHandler { Options = this.appRetryOptions, InnerHandler = httpMessageHandler };

            var client = this.appRetryOptions.Value.HandlerEnabled
                             ? new HttpClient(retryMessageHandler)
                             : new HttpClient(httpMessageHandler);

            this.SetDefaults(client);
            this.SetVersioningStuff(client,  preferredVersion ?? this.versionService.CurrentVersion);

            return client;
        }

        /// <summary>The set defaults.</summary>
        /// <param name="client">The client.</param>
        private void SetDefaults(HttpClient client)
        {
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        /// <summary>The set versioning stuff.</summary>
        /// <param name="client">The client.</param>
        /// <param name="apiVersion">The get requested API version.</param>
        private void SetVersioningStuff(HttpClient client, ApiVersion apiVersion)
        {
            // Do some versioning magic here
            switch (apiVersion.MajorVersion)
            {
                case 1: break;

                case 2: break;

                default:
                    break;
            }
        }
    }
}