﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventsClient.cs" company="">
//   
// </copyright>
// <summary>
//   Defines the EventsClient type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Core.RestClients
{
    using System;
    using System.Net.Http;
    using System.Threading.Tasks;

    using Microsoft.Extensions.Logging;

    using Prototype.Core.Auth;
    using Prototype.Core.Http;
    using Prototype.Core.Model;
    using Prototype.Core.Serialization;

    /// <summary>The events client.</summary>
    public class EventsClient : ClientBase, IEventsClient
    {
        /// <summary>The events path.</summary>
        public static readonly string EventsPath = "me/calendarview";

        /// <summary>Initializes a new instance of the <see cref="EventsClient" /> class.</summary>
        /// <param name="httpClientFactory">The http client factory.</param>
        /// <param name="logger">The logger.</param>
        /// <param name="tokenProvider">The token provider.</param>
        /// <param name="jsonSerializer">The JSON serializer.</param>
        public EventsClient(
            IHttpClientFactory httpClientFactory,
            ILogger<EventsClient> logger,
            ITokenProvider tokenProvider,
            IJsonSerializer jsonSerializer)
            : base(httpClientFactory, logger, tokenProvider, jsonSerializer)
        {
        }

        public async Task<ResponseModel<EventModel>> GetEvents()
        {
            var requestUri = new Uri($"{Constants.OutlookBaseAddress}{EventsPath}");

            return await this.Request<ResponseModel<EventModel>>(HttpMethod.Get, requestUri.ToString());
        }
    }
}