﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IEventsClient.cs" company="">
//   
// </copyright>
// <summary>
//   The EventsClient interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Core.RestClients
{
    using System;
    using System.Threading.Tasks;

    using Prototype.Core.Model;

    /// <summary>The EventsClient interface.</summary>
    public interface IEventsClient
    {
        /// <returns>The <see cref="Task" />.</returns>
        Task<ResponseModel<EventModel>> GetEvents();
    }
}