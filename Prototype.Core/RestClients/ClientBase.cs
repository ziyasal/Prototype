﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClientBase.cs" company="">
//   
// </copyright>
// <summary>
//   The client base.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Core.RestClients
{
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Threading.Tasks;

    using Microsoft.Extensions.Logging;

    using Prototype.Core.Auth;
    using Prototype.Core.Http;
    using Prototype.Core.Serialization;

    /// <summary>The client base.</summary>
    public abstract class ClientBase
    {
        /// <summary>The _http client factory.</summary>
        private readonly IHttpClientFactory httpClientFactory;

        /// <summary>The _logger.</summary>
        private readonly ILogger<ClientBase> logger;

        /// <summary>The _token provider.</summary>
        private readonly ITokenProvider tokenProvider;

        /// <summary>The json serializer.</summary>
        private readonly IJsonSerializer jsonSerializer;

        /// <summary>Initializes a new instance of the <see cref="ClientBase" /> class.</summary>
        /// <param name="httpClientFactory">The http client factory.</param>
        /// <param name="logger">The logger.</param>
        /// <param name="tokenProvider">The token provider.</param>
        /// <param name="jsonSerializer">The JSON serializer.</param>
        protected ClientBase(
            IHttpClientFactory httpClientFactory,
            ILogger<ClientBase> logger,
            ITokenProvider tokenProvider,
            IJsonSerializer jsonSerializer)
        {
            this.httpClientFactory = httpClientFactory;
            this.logger = logger;
            this.tokenProvider = tokenProvider;
            this.jsonSerializer = jsonSerializer;
        }

        /// <summary>The request.</summary>
        /// <param name="httpMethod">The http method.</param>
        /// <param name="requestUri">The request uri.</param>
        /// <typeparam name="T">The T.</typeparam>
        /// <returns>The <see cref="Task" />.</returns>
        protected async Task<T> Request<T>(HttpMethod httpMethod, string requestUri)
        {
            using (var request = new HttpRequestMessage(httpMethod, requestUri))
            {
                var accessToken = this.tokenProvider.GetToken();
                request.Headers.Authorization = new AuthenticationHeaderValue("bearer", accessToken);

                using (var response = await this.httpClientFactory.GetClient().SendAsync(request).ConfigureAwait(false))
                {
                    if (!response.IsSuccessStatusCode)
                    {
                        this.logger.LogWarning($"An error occurred while fetching, req: {requestUri}");
                    }

                    using (var httpContent = response.Content)
                    {
                        var content = await httpContent.ReadAsStringAsync().ConfigureAwait(false);
                        return this.jsonSerializer.Deserialize<T>(content);
                    }
                }
            }
        }
    }
}