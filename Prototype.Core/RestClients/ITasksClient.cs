﻿namespace Prototype.Core.RestClients
{
    using System;
    using System.Threading.Tasks;

    using Prototype.Core.Model;

    public interface ITasksClient
    {
        Task<ResponseModel<TaskModel>> GetTasks();
    }
}