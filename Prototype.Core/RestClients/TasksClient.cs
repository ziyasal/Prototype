﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TasksClient.cs" company="">
//   
// </copyright>
// <summary>
//   The tasks client.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Core.RestClients
{
    using System;
    using System.Net.Http;
    using System.Threading.Tasks;

    using Microsoft.Extensions.Logging;

    using Prototype.Core.Auth;
    using Prototype.Core.Http;
    using Prototype.Core.Model;
    using Prototype.Core.Serialization;

    /// <summary>The tasks client.</summary>
    public class TasksClient : ClientBase, ITasksClient
    {
        /// <summary>The tasks path.</summary>
        private static readonly string TasksPath = "me/tasks";

        /// <summary>Initializes a new instance of the <see cref="TasksClient"/> class.</summary>
        /// <param name="httpClientFactory">The http client factory.</param>
        /// <param name="logger">The logger.</param>
        /// <param name="tokenProvider">The token provider.</param>
        /// <param name="jsonSerializer">The json serializer.</param>
        public TasksClient(
            IHttpClientFactory httpClientFactory,
            ILogger<TasksClient> logger,
            ITokenProvider tokenProvider,
            IJsonSerializer jsonSerializer)
            : base(httpClientFactory, logger, tokenProvider, jsonSerializer)
        {
        }

        public async Task<ResponseModel<TaskModel>> GetTasks()
        {
            var requestUri = new Uri($"{Constants.OutlookBaseAddress}{TasksPath}");

            return await this.Request<ResponseModel<TaskModel>>(HttpMethod.Get, requestUri.ToString());
        }
    }
}