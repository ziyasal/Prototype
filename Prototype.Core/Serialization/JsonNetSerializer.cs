﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NewtonsoftJsonSerializer.cs" company="Hypless">
//   Hypless
// </copyright>
// <summary>
//   Defines the NewtonsoftJsonSerializer type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Core.Serialization
{
    using Newtonsoft.Json;

    /// <summary>The JSON net serializer.</summary>
    public class NewtonsoftJsonSerializer : IJsonSerializer
    {
        /// <inheritdoc />
        public T Deserialize<T>(string json, JsonSerializerSettings settings = null)
        {
            return JsonConvert.DeserializeObject<T>(json, settings ?? new JsonSerializerSettings());
        }

        /// <inheritdoc />
        public string Serialize<T>(T value, JsonSerializerSettings settings = null)
        {
            return JsonConvert.SerializeObject(value, settings ?? new JsonSerializerSettings());
        }
    }
}