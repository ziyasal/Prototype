﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IJsonSerializer.cs" company="Hypless">
//   Hypless
// </copyright>
// <summary>
//   The JsonSerializer interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Core.Serialization
{
    using Newtonsoft.Json;

    /// <summary>The JSON Serializer interface.</summary>
    public interface IJsonSerializer
    {
        /// <summary>The deserialize.</summary>
        /// <param name="json">The JSON.</param>
        /// <param name="settings">The settings.</param>
        /// <typeparam name="T">The T.</typeparam>
        /// <returns>The <see cref="T" />.</returns>
        T Deserialize<T>(string json, JsonSerializerSettings settings = null);

        /// <summary>The serialize.</summary>
        /// <param name="value">The value.</param>
        /// <param name="settings">The settings.</param>
        /// <typeparam name="T">The T.</typeparam>
        /// <returns>The <see cref="string" />.</returns>
        string Serialize<T>(T value, JsonSerializerSettings settings = null);
    }
}