﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BearerTokenProvider.cs" company="">
//   
// </copyright>
// <summary>
//   The token provider.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Core.Auth
{
    using System;

    /// <summary>The token provider.</summary>
    public class BearerTokenProvider : ITokenProvider
    {
        /// <summary>The get token.</summary>
        /// <returns>The <see cref="string"/>.</returns>
        public string GetToken()
        {
            return Guid.NewGuid().ToString();
        }
    }
}