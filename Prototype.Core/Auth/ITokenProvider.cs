﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITokenProvider.cs" company="">
//   
// </copyright>
// <summary>
//   The BearerTokenProvider interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Core.Auth
{
    /// <summary>The BearerTokenProvider interface.</summary>
    public interface ITokenProvider
    {
        /// <summary>The get token.</summary>
        /// <returns>The <see cref="string"/>.</returns>
        string GetToken();
    }
}