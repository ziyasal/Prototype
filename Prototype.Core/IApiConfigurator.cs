﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IApiConfigurator.cs" company="Hypless">
//   Hypless
// </copyright>
// <summary>
//   The ApiConfigurator interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Core
{
    using System.Web.Http;

    /// <summary>The API configurator interface.</summary>
    public interface IApiConfigurator
    {
        /// <summary>The configure.</summary>
        /// <param name="config">The config.</param>
        void Configure(HttpConfiguration config);
    }
}