// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TodosV2Response.cs" company="">
//   
// </copyright>
// <summary>
//   The to-dos v2 response.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Core.Contracts.V2.Response
{
    using System.Collections.Generic;

    /// <summary>The to-dos v2 response.</summary>
    public class TodosV2Response : IClientResponse
    {
        /// <summary>Gets or sets the tasks.</summary>
        public List<TaskV2ClientModel> Tasks { get; set; }

        /// <summary>Gets or sets the events.</summary>
        public List<EventV2ClientModel> Events { get; set; }
    }
}