namespace Prototype.Core.Contracts.V2
{
    using System;

    using Microsoft.Web.Http;

    /// <summary>The event client model.</summary>

    public class EventV2ClientModel
    {
        public DateTime End { get; set; }
        public DateTime Start { get; set; }

        public string Id { get; set; }

        public double NewField { get; set; }
    }
}