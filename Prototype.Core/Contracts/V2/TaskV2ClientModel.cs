// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TaskClientModel.cs" company="">
//   
// </copyright>
// <summary>
//   The task client model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Core.Contracts.V2
{
    using Microsoft.Web.Http;

    /// <summary>The task client model.</summary>
    public class TaskV2ClientModel
    {
        public string Id { get; set; }

        public string Subject { get; set; }
    }
}