﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MappingV2Profile.cs" company="">
//   
// </copyright>
// <summary>
//   The mapping engine.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Core.Contracts.V2.Mapping
{
    using ExpressMapper;

    using Microsoft.Web.Http;

    using Prototype.Core.Contracts.V2;
    using Prototype.Core.Mapping;
    using Prototype.Core.Model;

    /// <summary>The v2 mapping profile</summary>
    [ApiVersion("2.0")]
    public class MappingV2Profile : IMappingProfile
    {
        /// <summary>The build mapping service provider.</summary>
        /// <returns>The <see cref="IMappingServiceProvider"/>.</returns>
        public IMappingServiceProvider GetMappingProvider()
        {
            var mapper = new MappingServiceProvider();

            mapper.RegisterCustom<TaskModel, TaskV2ClientModel, TaskV2ClientModelMapper>();
            mapper.RegisterCustom<EventModel, EventV2ClientModel, EventV2ClientModelMapper>();

            return mapper;
        }
    }
}