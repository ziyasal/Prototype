﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TaskV2ClientModelMapper.cs" company="">
//   
// </copyright>
// <summary>
//   The custom mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Core.Contracts.V2.Mapping
{
    using ExpressMapper;

    using Microsoft.Web.Http;

    using Prototype.Core.Model;

    /// <summary>The custom mapper.</summary>
    [ApiVersion("2.0")]
    public class TaskV2ClientModelMapper : ICustomTypeMapper<TaskModel, TaskV2ClientModel>
    {
        /// <summary>The map.</summary>
        /// <param name="ctx">The context.</param>
        /// <returns>The <see cref="TaskModel" />.</returns>
        public TaskV2ClientModel Map(IMappingContext<TaskModel, TaskV2ClientModel> ctx)
        {
            return new TaskV2ClientModel
            {
                Id = ctx.Source.Id,
                Subject = $"MAPPER-V2 - {ctx.Source.Subject}"
            };
        }
    }
}