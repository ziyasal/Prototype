﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventV2ClientModelMapper.cs" company="Hypless">
//   Hypless
// </copyright>
// <summary>
//   The custom mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Core.Contracts.V2.Mapping
{
    using ExpressMapper;

    using Microsoft.Web.Http;

    using Prototype.Core.Model;

    /// <summary>The custom mapper.</summary>
    [ApiVersion("2.0")]
    public class EventV2ClientModelMapper : ICustomTypeMapper<EventModel, EventV2ClientModel>
    {
        /// <summary>The map.</summary>
        /// <param name="ctx">The context.</param>
        /// <returns>The <see cref="TaskModel" />.</returns>
        public EventV2ClientModel Map(IMappingContext<EventModel, EventV2ClientModel> ctx)
        {
            return new EventV2ClientModel
            {
                Id = ctx.Source.Id,
                Start = ctx.Source.Start,
                NewField = ctx.Source.NewField,
                End = ctx.Source.End
            };
        }
    }
}