// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TaskV1ClientModel.cs" company="">
//   
// </copyright>
// <summary>
//   The task client model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Core.Contracts.V1
{
    using Microsoft.Web.Http;

    /// <summary>The task client model.</summary>
    public class TaskV1ClientModel
    {
        public string Id { get; set; }

        public string Subject { get; set; }
    }
}