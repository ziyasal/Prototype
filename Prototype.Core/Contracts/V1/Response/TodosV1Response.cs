// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TodosV1Response.cs" company="Hypless">
//   Hypless
// </copyright>
// <summary>
//   Defines the TodosV1Response type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Core.Contracts.V1.Response
{
    using System.Collections.Generic;

    using Newtonsoft.Json;

    /// <summary>The To Do view.</summary>
    public class TodosV1Response : IClientResponse
    {
        // omitted for brevity.

        /// <summary>Dummy Id field.</summary>
        [JsonIgnore]
        public int Id { get; set; }

        /// <summary>Gets or sets the events.</summary>
        public List<EventV1ClientModel> Events { get; set; }

        /// <summary>Gets or sets the tasks.</summary>
        public List<TaskV1ClientModel> Tasks { get; set; }
    }
}