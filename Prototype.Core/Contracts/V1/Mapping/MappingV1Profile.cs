﻿namespace Prototype.Core.Contracts.V1.Mapping
{
    using ExpressMapper;

    using Microsoft.Web.Http;

    using Prototype.Core.Contracts.V1;
    using Prototype.Core.Mapping;
    using Prototype.Core.Model;

    /// <summary>The v1 mapping profile.</summary>
    [ApiVersion("1.0")]
    public class MappingV1Profile : IMappingProfile
    {
        /// <summary>The build mapping service provider.</summary>
        /// <returns>The <see cref="IMappingServiceProvider"/>.</returns>
        public IMappingServiceProvider GetMappingProvider()
        {
            var mapper = new MappingServiceProvider();

            mapper.RegisterCustom<TaskModel, TaskV1ClientModel, TaskV1ClientModelMapper>();
            mapper.RegisterCustom<EventModel, EventV1ClientModel, EventV1ClientModelMapper>();

            return mapper;
        }
    }
}
