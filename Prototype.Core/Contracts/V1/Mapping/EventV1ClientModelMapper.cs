﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventV1ClientModelMapper.cs" company="">
//   
// </copyright>
// <summary>
//   The custom mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Core.Contracts.V1.Mapping
{
    using ExpressMapper;

    using Microsoft.Web.Http;

    using Prototype.Core.Model;

    /// <summary>The custom mapper.</summary>
    [ApiVersion("1.0")]
    public class EventV1ClientModelMapper : ICustomTypeMapper<EventModel, EventV1ClientModel>
    {
        /// <summary>The map.</summary>
        /// <param name="ctx">The context.</param>
        /// <returns>The <see cref="TaskModel" />.</returns>
        public EventV1ClientModel Map(IMappingContext<EventModel, EventV1ClientModel> ctx)
        {
            return new EventV1ClientModel
            {
                Id = ctx.Source.Id,
                Subject = $"MAPPER-V1 - {ctx.Source.Subject}",
                Start = ctx.Source.Start,
                End = ctx.Source.End
            };
        }
    }
}