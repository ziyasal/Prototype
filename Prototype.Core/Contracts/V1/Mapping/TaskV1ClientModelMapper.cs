﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TaskV1ClientModelMapper.cs" company="">
//   
// </copyright>
// <summary>
//   The custom mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Core.Contracts.V1.Mapping
{
    using ExpressMapper;

    using Microsoft.Web.Http;

    using Prototype.Core.Model;

    /// <summary>The custom mapper.</summary>
    [ApiVersion("1.0")]
    public class TaskV1ClientModelMapper : ICustomTypeMapper<TaskModel, TaskV1ClientModel>
    {
        /// <summary>The map.</summary>
        /// <param name="ctx">The context.</param>
        /// <returns>The <see cref="TaskModel" />.</returns>
        public TaskV1ClientModel Map(IMappingContext<TaskModel, TaskV1ClientModel> ctx)
        {
            return new TaskV1ClientModel { Id = ctx.Source.Id, Subject = $"MAPPER-V1 - {ctx.Source.Subject}" };
        }
    }
}