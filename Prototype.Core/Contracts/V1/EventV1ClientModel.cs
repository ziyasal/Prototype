namespace Prototype.Core.Contracts.V1
{
    using System;

    using Microsoft.Web.Http;

    /// <summary>The event client model.</summary>
    public class EventV1ClientModel
    {
        public DateTime End { get; set; }

        public string Id { get; set; }

        public DateTime Start { get; set; }

        public string Subject { get; set; }
    }
}