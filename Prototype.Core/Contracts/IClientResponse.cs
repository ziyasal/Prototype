// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IClientResponse.cs" company="">
//   
// </copyright>
// <summary>
//   Defines the IClientResponse type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Core.Contracts
{
    /// <summary>The Response interface.</summary>
    public interface IClientResponse
    {
    }
}