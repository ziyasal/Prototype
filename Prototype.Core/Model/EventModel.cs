﻿namespace Prototype.Core.Model
{
    using System;

    public class EventModel
    {
        public DateTime End { get; set; }

        public string Id { get; set; }

        public DateTime Start { get; set; }

        // Deleted in v2
        public string Subject { get; set; }

        // Added in v2
        public double NewField { get; set; }
    }
}