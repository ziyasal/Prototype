﻿namespace Prototype.Core.Model
{
    using System.Collections.Generic;

    public class ResponseModel<T>
    {
        public List<T> Value { get; set; }
    }
}