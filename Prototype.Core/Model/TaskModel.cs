﻿namespace Prototype.Core.Model
{
    public class TaskModel
    {
        public string Id { get; set; }

        public string Subject { get; set; }
    }
}