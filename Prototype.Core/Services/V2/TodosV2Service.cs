﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TodosV2Service.cs" company="">
//   
// </copyright>
// <summary>
//   The to-do service v2.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Core.Services.V2
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using ExpressMapper;

    using Microsoft.Extensions.Logging;
    using Microsoft.Web.Http;

    using Optional;

    using Prototype.Core.Contracts;
    using Prototype.Core.Contracts.V2;
    using Prototype.Core.Contracts.V2.Response;
    using Prototype.Core.Model;
    using Prototype.Core.RestClients;

    /// <summary>The to-do service v2.</summary>
    [ApiVersion("2.0")]

    public class TodosV2Service : ITodosService
    {
        /// <summary>The events client.</summary>
        private readonly IEventsClient eventsClient;

        /// <summary>The tasks client.</summary>
        private readonly ITasksClient tasksClient;

        /// <summary>The logger.</summary>
        private readonly ILogger<TodosV2Service> logger;

        /// <summary>The mapping service provider.</summary>
        private readonly IMappingServiceProvider mapper;

        /// <summary>Initializes a new instance of the <see cref="TodosV2Service"/> class.</summary>
        /// <param name="eventsClient">The events client.</param>
        /// <param name="tasksClient">The tasks client.</param>
        /// <param name="logger">The logger.</param>
        /// <param name="mapper">The mapping Service Provider.</param>
        public TodosV2Service(
            IEventsClient eventsClient,
            ITasksClient tasksClient,
            ILogger<TodosV2Service> logger,
            IMappingServiceProvider mapper)
        {
            this.eventsClient = eventsClient;
            this.tasksClient = tasksClient;
            this.logger = logger;
            this.mapper = mapper;
        }

        /// <summary>The get to-dos.</summary>
        /// <returns>The <see cref="Task"/>.</returns>
        public async Task<Option<IClientResponse>> GetTodos()
        {
            try
            {
                var eventsTask = this.eventsClient.GetEvents();

                var tasksTask = this.tasksClient.GetTasks();

                await Task.WhenAll(eventsTask, tasksTask);

                tasksTask.Result.Value.ForEach(model => model.Subject = "TodosService v2");

                var todosResponse = new TodosV2Response
                {
                    Tasks = this.mapper.Map<List<TaskModel>, List<TaskV2ClientModel>>(
                                                        tasksTask.Result.Value),
                    Events = this.mapper.Map<List<EventModel>, List<EventV2ClientModel>>(
                                                    eventsTask.Result.Value)
                };

                return Option.Some<IClientResponse>(todosResponse);
            }
            catch (Exception e)
            {
                this.logger.LogError("An error occurred while getting tasks and events", e);
                return Option.None<IClientResponse>();
            }
        }
    }
}