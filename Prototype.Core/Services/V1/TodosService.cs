﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TodosService.cs" company="">
//   
// </copyright>
// <summary>
//   The to-do service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Core.Services.V1
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using ExpressMapper;

    using Microsoft.Extensions.Logging;
    using Microsoft.Web.Http;

    using Optional;

    using Prototype.Core.Contracts;
    using Prototype.Core.Contracts.V1;
    using Prototype.Core.Contracts.V1.Response;
    using Prototype.Core.Model;
    using Prototype.Core.RestClients;

    /// <summary>The to-do service.</summary>
    [ApiVersion("1.0")]
    public class TodosService : ITodosService
    {
        /// <summary>The events client.</summary>
        private readonly IEventsClient eventsClient;

        /// <summary>The logger.</summary>
        private readonly ILogger<TodosService> logger;

        /// <summary>The mapping service provider.</summary>
        private readonly IMappingServiceProvider mapper;

        /// <summary>The tasks client.</summary>
        private readonly ITasksClient tasksClient;

        /// <summary>Initializes a new instance of the <see cref="TodosService"/> class.</summary>
        /// <param name="eventsClient">The events client.</param>
        /// <param name="tasksClient">The tasks client.</param>
        /// <param name="logger">The logger.</param>
        /// <param name="mapper">The mapping Service Provider.</param>
        public TodosService(
            IEventsClient eventsClient,
            ITasksClient tasksClient,
            ILogger<TodosService> logger,
            IMappingServiceProvider mapper)
        {
            this.eventsClient = eventsClient;
            this.tasksClient = tasksClient;
            this.logger = logger;
            this.mapper = mapper;
        }

        /// <summary>The get to-do's.</summary>
        /// <returns>The <see cref="Task"/>.</returns>
        public virtual async Task<Option<IClientResponse>> GetTodos()
        {
            try
            {
                var eventsTask = this.eventsClient.GetEvents();

                var tasksTask = this.tasksClient.GetTasks();

                await Task.WhenAll(eventsTask, tasksTask);

                var response = new TodosV1Response
                {
                    Tasks = this.mapper.Map<List<TaskModel>, List<TaskV1ClientModel>>(
                                               tasksTask.Result.Value),
                    Events = this.mapper.Map<List<EventModel>, List<EventV1ClientModel>>(
                                               eventsTask.Result.Value)
                };
                return Option.Some<IClientResponse>(response);
            }
            catch (Exception e)
            {
                this.logger.LogError("An error occurred while getting tasks and events", e);
                return Option.None<IClientResponse>();
            }
        }
    }
}