﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITodosService.cs" company="">
//   
// </copyright>
// <summary>
//   The TodosService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Core.Services
{
    using System.Threading.Tasks;

    using Optional;

    using Prototype.Core.Contracts;

    /// <summary>The To-do Service interface.</summary>
    public interface ITodosService
    {
        /// <summary>The get to-do's.</summary>
        /// <returns>The <see cref="Task"/>.</returns>
        Task<Option<IClientResponse>> GetTodos();
    }
}