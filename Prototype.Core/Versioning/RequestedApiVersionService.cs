﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HttpContextVersionService.cs" company="">
//   
// </copyright>
// <summary>
//   The http context version service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Core.Versioning
{
    using System.Net.Http;
    using System.Web.Http;

    using Microsoft.Web.Http;

    using Prototype.Core.Http;

    /// <summary>The version service.</summary>
    public class RequestedApiVersionService : IVersionService
    {
        /// <summary>The http request message key.</summary>
        private const string HttpRequestMessageKey = "MS_HttpRequestMessage";

        /// <summary>The http context accessor.</summary>
        private readonly IHttpContextAccessor httpContextAccessor;

        /// <summary>Initializes a new instance of the <see cref="RequestedApiVersionService"/> class.</summary>
        /// <param name="httpContextAccessor">The http context accessor.</param>
        public RequestedApiVersionService(IHttpContextAccessor httpContextAccessor)
        {
            this.httpContextAccessor = httpContextAccessor;
        }

        /// <summary>The current version.</summary>
        public ApiVersion CurrentVersion => this.ExtractVersion();

        /// <summary>The default version.</summary>
        public ApiVersion DefaultVersion => new ApiVersion(1, 0);

        /// <summary>The extract version.</summary>
        /// <returns>The <see cref="ApiVersion"/>.</returns>
        private ApiVersion ExtractVersion()
        {
            if (this.httpContextAccessor.HttpContext.Items.Contains(HttpRequestMessageKey))
            {
                var currentRequestMessage =
                    (HttpRequestMessage)this.httpContextAccessor.HttpContext.Items[HttpRequestMessageKey];

                return currentRequestMessage.GetRequestedApiVersion();
            }

            return ApiVersion.Default;
        }
    }
}