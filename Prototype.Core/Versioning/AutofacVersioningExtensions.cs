﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AutofacVersioningExtensions.cs" company="">
//   
// </copyright>
// <summary>
//   The Auto-fac versioning extensions.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Core.Versioning
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    using Autofac;
    using Autofac.Builder;
    using Autofac.Core;

    using ExpressMapper;

    using Microsoft.Web.Http;

    using Prototype.Core.Mapping;

    /// <summary>The Auto-fac versioning extensions.</summary>
    public static class AutofacVersioningExtensions
    {
        /// <summary>The register versioned.</summary>
        /// <param name="builder">The builder.</param>
        /// <typeparam name="T">The T.</typeparam>
        /// <returns>The <see cref="IRegistrationBuilder{T, ConcreteReflectionActivatorData, SingleRegistrationStyle}" />.</returns>
        public static IRegistrationBuilder<T, ConcreteReflectionActivatorData, SingleRegistrationStyle> RegisterVersioned<T>(this ContainerBuilder builder)
        {
            var parameters = CreateParameterList<T>();

            return builder
                .RegisterType<T>()
                .WithParameters(parameters);
        }

        /// <summary>The register versioned.</summary>
        /// <param name="builder">The builder.</param>
        /// <typeparam name="T">The T.</typeparam>
        /// <typeparam name="TService">T Service.</typeparam>
        /// <returns>The <see cref="IRegistrationBuilder{T, ConcreteReflectionActivatorData, SingleRegistrationStyle}" />.</returns>
        public static IRegistrationBuilder<T, ConcreteReflectionActivatorData, SingleRegistrationStyle> RegisterVersioned<T, TService>(this ContainerBuilder builder)
        {
            var parameters = CreateParameterList<T>();

            var versionAttribute = typeof(T).GetCustomAttribute<ApiVersionAttribute>();

            if (versionAttribute != null)
            {
                var serviceKey = versionAttribute.Versions.First().ToString();

                return builder
                    .RegisterType<T>()
                    .Keyed(serviceKey, typeof(TService))
                    .WithParameters(parameters);
            }

            return builder.RegisterType<T>().WithParameters(parameters);
        }

        /// <summary>The register versioned.</summary>
        /// <param name="builder">The builder.</param>
        /// <typeparam name="T">The T.</typeparam>
        /// <typeparam name="TProfile">The mapping profile.</typeparam>
        /// <returns>The <see cref="IRegistrationBuilder{T, ConcreteReflectionActivatorData, SingleRegistrationStyle}"/>.</returns>
        public static IRegistrationBuilder<T, SimpleActivatorData, SingleRegistrationStyle>
            RegisterVersionedMapper<T, TProfile>(this ContainerBuilder builder) where T : class, IMappingServiceProvider
        {
            var mappingProfile = (IMappingProfile)Activator.CreateInstance<TProfile>();

            var versionAttribute = typeof(TProfile).GetCustomAttribute<ApiVersionAttribute>();

            var serviceKey = versionAttribute.Versions.First().ToString();

            var mappingProvider = (T)mappingProfile.GetMappingProvider();

            // Precompile all registered mappings no compilation during first time mapping
            mappingProvider.Compile();

            return builder
                .RegisterInstance(mappingProvider)
                .Keyed<IMappingServiceProvider>(serviceKey)
                .SingleInstance();
        }

        /// <summary>The create parameter list.</summary>
        /// <typeparam name="T">The T.</typeparam>
        /// <returns>The <see cref="List{T}" />.</returns>
        private static Parameter[] CreateParameterList<T>()
        {
            var ctor = typeof(T).GetConstructors().Where(c => c.IsPublic)
                .OrderByDescending(t => t.GetParameters().Count()).First();

            var parameters = new List<Parameter>();

            foreach (var parameter in ctor.GetParameters())
            {
                parameters.Add(new ResolvedParameter((_, __) => true, ResolveWithVersioning));
            }

            return parameters.ToArray();
        }

        /// <summary>The resolve with versioning.</summary>
        /// <param name="pi">The pi.</param>
        /// <param name="ctx">The context.</param>
        /// <returns>The <see cref="object" />.</returns>
        private static object ResolveWithVersioning(ParameterInfo pi, IComponentContext ctx)
        {
            var versionService = ctx.Resolve<IVersionService>();
            var version = versionService.CurrentVersion.ToString() ?? versionService.DefaultVersion.ToString();

            if (ctx.TryResolveKeyed(version, pi.ParameterType, out var result))
            {
                return result;
            }

            return ctx.Resolve(pi.ParameterType);
        }
    }
}