﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IVersionService.cs" company="">
//   
// </copyright>
// <summary>
//   The VersionService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Core.Versioning
{
    using Microsoft.Web.Http;

    /// <summary>The VersionService interface.</summary>
    public interface IVersionService
    {
        /// <summary>Gets the current version.</summary>
        ApiVersion CurrentVersion { get; }

        /// <summary>Gets the default version.</summary>
        ApiVersion DefaultVersion { get; }
    }
}