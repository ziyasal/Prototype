Prototype
=================

⚠️ **w.i.p**

## Some Features
- Integrated DI (Autofac) with extended versioning support and also integrates with Microsoft ASP.NET API Versioning libraries
- OData support with versioning
- Multiple API projects (versions can live in different projects) in one hosting (matches with our case)
- Shared  Core layer
- Versioned Models and converter profiles (uses ExpressMapper)
- Versioned HttpClient factory
- etc

Plus (later on);
- Unit testing with handy HTTP mocking
- Versioned testing
- Integration testing with advanced test hosting

**Briefly**
```
                         Request
                            |
                    Asp.NET Versioning { OData & Web API }
                            |
                        Controller { OData & Web API }
                            |
                    _ _ Services  _ _ 
                  /          |         \
            Mapper     Other Deps     Rest Clients 
             |                        /           \
    Mapping profiles           Token Provider    Http Client Factory 
     - [TODO]Auto build                           - version support
     - Custom mappers                             - prefer version support
                                                    (create a client configured for specific version)
```
