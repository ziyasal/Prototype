﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ApiConfigurator.cs" company="Hypless">
//   Hypless
// </copyright>
// <summary>
//   The bus api config.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Api.OData
{
    using System.Net.Http.Formatting;
    using System.Web.Http;
    using System.Web.Http.Routing;

    using Microsoft.Web.Http.Routing;
    using Microsoft.Web.OData.Builder;

    using Prototype.Core;
    using Prototype.Core.Contracts.V1;
    using Prototype.Core.Contracts.V1.Response;

    /// <summary>The bus API config.</summary>
    public class ODataApiConfigurator : IApiConfigurator
    {
        /// <summary>The configure.</summary>
        /// <param name="config">The config.</param>
        public void Configure(HttpConfiguration config)
        {
            var modelBuilder = new VersionedODataModelBuilder(config)
            {
                DefaultModelConfiguration = (builder, apiVersion) =>
                    {
                        builder.EntitySet<TodosV1Response>("Todos").EntityType.HasKey(response => response.Id);
                    }
            };
            var models = modelBuilder.GetEdmModels();

            config.MapVersionedODataRoutes("odata", null, models);
        }
    }
}