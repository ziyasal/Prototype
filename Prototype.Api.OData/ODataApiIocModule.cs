﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ApiIocModule.cs" company="Hypless">
//   Hypless
// </copyright>
// <summary>
//   The bus module.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Api.OData
{
    using Autofac;

    using ExpressMapper;

    using Microsoft.Extensions.Options;

    using Prototype.Api.OData.Controllers.V1;
    using Prototype.Core.Auth;
    using Prototype.Core.Contracts.V1;
    using Prototype.Core.Contracts.V1.Mapping;
    using Prototype.Core.Contracts.V2;
    using Prototype.Core.Contracts.V2.Mapping;
    using Prototype.Core.Http;
    using Prototype.Core.Model;
    using Prototype.Core.RestClients;
    using Prototype.Core.Serialization;
    using Prototype.Core.Services;
    using Prototype.Core.Versioning;

    /// <summary>The bus module.</summary>
    public class ODataApiIocModule : Module
    {
        /// <summary>The load.</summary>
        /// <param name="builder">The builder.</param>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterVersioned<TodosODataController>();
        }
    }
}