﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TestDataHelper.cs" company="">
//   
// </copyright>
// <summary>
//   Defines the TestDataHelper type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Api.UnitTests
{
    using Newtonsoft.Json;

    using Prototype.Core.Model;

    /// <summary>The test data helper.</summary>
    public class TestDataHelper
    {
        /// <summary>The events.</summary>
        public static ResponseModel<EventModel> Events =>
            JsonConvert.DeserializeObject<ResponseModel<EventModel>>(EventsJson);

        /// <summary>The tasks.</summary>
        public static ResponseModel<TaskModel> Tasks =>
            JsonConvert.DeserializeObject<ResponseModel<TaskModel>>(TasksJson);

        /// <summary>The events JSON.</summary>
        public static string EventsJson => @"{
  ""@odata.context"": ""https://outlook.office.com/api/v2.0/$metadata#Me/CalendarView"",
  ""value"": [
    {
      ""@odata.id"":
        ""https://outlook.office.com/api/v2.0/Users('0003bffd-07f0-8eeb-0000-000000000000@84df9e7f-e9f6-40af-b435-aaaaaaaaaaaa')/Events('AQMkADAwATNiZmYAZC0wN2YwLThlZQBiLTAwAi0wMAoARgAAA-4LrrWNShVKr4lt0lhMDU8HAFohDV6Uuq9AoX8Dq4PZF1MAAAIBDQAAAFohDV6Uuq9AoX8Dq4PZF1MAAAINLQAAAA==')"",
      ""@odata.etag"": ""W/\""WiENXpS6r0ChfwOrg9kXUwAAAAAQiQ==\"""",
      ""Id"":
        ""AQMkADAwATNiZmYAZC0wN2YwLThlZQBiLTAwAi0wMAoARgAAA-4LrrWNShVKr4lt0lhMDU8HAFohDV6Uuq9AoX8Dq4PZF1MAAAIBDQAAAFohDV6Uuq9AoX8Dq4PZF1MAAAINLQAAAA=="",
      ""CreatedDateTime"": ""2017-07-23T12:54:02.9016552Z"",
      ""LastModifiedDateTime"": ""2017-07-23T12:54:40.6710568Z"",
      ""ChangeKey"": ""WiENXpS6r0ChfwOrg9kXUwAAAAAQiQ=="",
      ""Categories"": [],
      ""OriginalStartTimeZone"": ""UTC"",
      ""OriginalEndTimeZone"": ""UTC"",
      ""iCalUId"":
        ""040000008200E00074C5B7101A82E008000000002DDBACC2B203D3010000000000000000100000009103C8725EB07B42B563124FE59D82E7"",
      ""ReminderMinutesBeforeStart"": 15,
      ""IsReminderOn"": false,
      ""HasAttachments"": false,
      ""Subject"": ""Test Event"",
      ""BodyPreview"": ""First Event July 23"",
      ""Importance"": ""Normal"",
      ""Sensitivity"": ""Normal"",
      ""IsAllDay"": true,
      ""IsCancelled"": false,
      ""IsOrganizer"": true,
      ""ResponseRequested"": true,
      ""SeriesMasterId"": null,
      ""ShowAs"": ""Free"",
      ""Type"": ""SingleInstance"",
      ""WebLink"":
        ""https://outlook.live.com/owa/?itemid=AQMkADAwATNiZmYAZC0wN2YwLThlZQBiLTAwAi0wMAoARgAAA%2F4LrrWNShVKr4lt0lhMDU8HAFohDV6Uuq9AoX8Dq4PZF1MAAAIBDQAAAFohDV6Uuq9AoX8Dq4PZF1MAAAINLQAAAA%3D%3D&exvsurl=1&path=/calendar/item"",
      ""OnlineMeetingUrl"": null,
      ""ResponseStatus"": {
        ""TodosApiResponse"": ""Organizer"",
        ""Time"": ""0001-01-01T00:00:00Z""
      },
      ""Body"": {
        ""ContentType"": ""HTML"",
        ""Content"": """"
      },
      ""Start"": {
        ""DateTime"": ""2017-07-23T00:00:00.0000000"",
        ""TimeZone"": ""UTC""
      },
      ""End"": {
        ""DateTime"": ""2017-07-24T00:00:00.0000000"",
        ""TimeZone"": ""UTC""
      },
      ""Location"": {
        ""DisplayName"": ""Berlin"",
        ""Address"": {
          ""Type"": ""Unknown"",
          ""Street"": """",
          ""City"": ""Berlin"",
          ""State"": ""Berlin"",
          ""CountryOrRegion"": ""Deutschland"",
          ""PostalCode"": """"
        },
        ""Coordinates"": {
          ""Latitude"": 52.5161,
          ""Longitude"": 13.377
        }
      },
      ""Recurrence"": null,
      ""Attendees"": [],
      ""Organizer"": {
        ""EmailAddress"": {
          ""Name"": ""MSTest Account"",
          ""Address"": ""mscase23072017@outlook.com""
        }
      }
    }
  ]
}";

        /// <summary>The tasks JSON.</summary>
        public static string TasksJson => @"{
  ""@odata.context"": ""https://outlook.office.com/api/v2.0/$metadata#Me/Tasks"",
  ""value"": [
    {
      ""@odata.id"":
        ""https://outlook.office.com/api/v2.0/Users('0003bffd-07f0-8eeb-0000-000000000000@84df9e7f-e9f6-40af-b435-aaaaaaaaaaaa')/Tasks('AQMkADAwATNiZmYAZC0wN2YwLThlZQBiLTAwAi0wMAoARgAAA-4LrrWNShVKr4lt0lhMDU8HAFohDV6Uuq9AoX8Dq4PZF1MAAAIBEgAAAFohDV6Uuq9AoX8Dq4PZF1MAAAI0QAAAAA==')"",
      ""@odata.etag"": ""W/\""WiENXpS6r0ChfwOrg9kXUwAAAAA61A==\"""",
      ""Id"":
        ""AQMkADAwATNiZmYAZC0wN2YwLThlZQBiLTAwAi0wMAoARgAAA-4LrrWNShVKr4lt0lhMDU8HAFohDV6Uuq9AoX8Dq4PZF1MAAAIBEgAAAFohDV6Uuq9AoX8Dq4PZF1MAAAI0QAAAAA=="",
      ""CreatedDateTime"": ""2017-07-23T12:53:01.7147837Z"",
      ""LastModifiedDateTime"": ""2017-07-23T12:53:01.7518144Z"",
      ""ChangeKey"": ""WiENXpS6r0ChfwOrg9kXUwAAAAA61A=="",
      ""Categories"": [],
      ""AssignedTo"": """",
      ""HasAttachments"": false,
      ""Importance"": ""Normal"",
      ""IsReminderOn"": false,
      ""Owner"": ""MSTest Account"",
      ""ParentFolderId"":
        ""AQMkADAwATNiZmYAZC0wN2YwLThlZQBiLTAwAi0wMAoALgAAA-4LrrWNShVKr4lt0lhMDU8BAFohDV6Uuq9AoX8Dq4PZF1MAAAIBEgAAAA=="",
      ""Sensitivity"": ""Normal"",
      ""Status"": ""NotStarted"",
      ""Subject"": ""Test Task"",
      ""Body"": {
        ""ContentType"": ""HTML"",
        ""Content"":
          ""<html>\r\n<head>\r\n<meta http-equiv=\""Content-Type\"" content=\""text/html; charset=utf-8\"">\r\n<meta content=\""text/html; charset=iso-8859-1\"">\r\n<style type=\""text/css\"" style=\""display:none\"">\r\n<!--\r\np\r\n\t{margin-top:0;\r\n\tmargin-bottom:0}\r\n-->\r\n</style>\r\n</head>\r\n<body dir=\""ltr\"">\r\n<div id=\""divtagdefaultwrapper\"" dir=\""ltr\"" style=\""font-size:12pt; color:#000000; font-family:Calibri,Helvetica,sans-serif\"">\r\n<p>Content</p>\r\n</div>\r\n</body>\r\n</html>\r\n""
      },
      ""CompletedDateTime"": null,
      ""DueDateTime"": {
        ""DateTime"": ""2017-07-23T12:52:40.7640000"",
        ""TimeZone"": ""UTC""
      },
      ""Recurrence"": null,
      ""ReminderDateTime"": null,
      ""StartDateTime"": null
    }
  ]
}";
    }
}