// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MockHttpClientFactory.cs" company="">
//   
// </copyright>
// <summary>
//   The mock http client factory.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Prototype.Api.UnitTests
{
    using System.Net.Http;

    using Microsoft.Web.Http;

    using Prototype.Core;
    using Prototype.Core.Http;

    using RichardSzalay.MockHttp;

    /// <summary>The mock http client factory.</summary>
    public class MockHttpClientFactory : IHttpClientFactory
    {
        /// <summary>The get client.</summary>
        /// <param name="preferredVersion">The preferred Version.</param>
        /// <returns>The <see cref="HttpClient"/>.</returns>
        public HttpClient GetClient(ApiVersion preferredVersion = null)
        {
            var mockHttp = new MockHttpMessageHandler();

            mockHttp.When($"{Constants.OutlookBaseAddress}me/tasks")
                .Respond("application/json", TestDataHelper.TasksJson);

            mockHttp.When($"{Constants.OutlookBaseAddress}me/calendarview*").Respond(
                "application/json",
                TestDataHelper.EventsJson);

            var client = mockHttp.ToHttpClient();

            return client;
        }
    }
}